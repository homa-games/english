package su.smkt.english.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import su.smkt.english.ui.dictionary.DictionaryViewModel
import su.smkt.english.ui.irregular.IrregularWordsViewModel
import su.smkt.english.ui.lessons.LessonsViewModel
import su.smkt.english.ui.settings.SettingsViewModel

/**
 * Модули для внедрение зависимостей через koin
 */
val viewModelsModule = module {
    viewModel { DictionaryViewModel() }
    viewModel { IrregularWordsViewModel() }
    viewModel { LessonsViewModel() }
    viewModel { SettingsViewModel() }
}