package su.smkt.english

import android.app.Application
import android.widget.Toast
import androidx.annotation.StringRes
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import su.smkt.english.di.viewModelsModule

/**
 * Класс приложения
 */
class App : Application() {

    companion object {
        lateinit var INSTANCE: App
            private set

        fun showToast(@StringRes resId: Int) {
            Toast.makeText(INSTANCE, resId, Toast.LENGTH_LONG).show()
        }

        fun showToast(message: String, length: Int = Toast.LENGTH_LONG) {
            Toast.makeText(INSTANCE, message, length).show()
        }
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@App)
            modules(viewModelsModule)
        }
    }
}