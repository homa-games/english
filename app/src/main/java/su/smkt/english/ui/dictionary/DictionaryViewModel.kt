package su.smkt.english.ui.dictionary

import androidx.lifecycle.ViewModel

class DictionaryViewModel : ViewModel() {

    fun getSize(): Int = 10

    fun getEngWord(position: Int): String = "eng $position"

    fun getRusWord(position: Int): String = "rus $position"
}