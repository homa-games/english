package su.smkt.english.ui.dictionary

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.koin.androidx.viewmodel.ext.android.viewModel
import su.smkt.english.R

class DictionaryFragment : Fragment() {
    private val viewModel by viewModel<DictionaryViewModel>()
    private lateinit var rvWords: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dictionary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvWords = view.findViewById<RecyclerView>(R.id.rv_words).apply {
            adapter = WordsAdapter()
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private inner class WordsAdapter : RecyclerView.Adapter<WordViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
            return WordViewHolder(LayoutInflater.from(parent.context)
                                                .inflate(R.layout.card_word, parent, false))
        }

        override fun getItemCount(): Int {
            return viewModel.getSize()
        }

        override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
            holder.apply {
                itemView.setOnClickListener { Log.d("123", "click") }
                tvEng.text = viewModel.getEngWord(position)
                tvRus.text = viewModel.getRusWord(position)
            }
        }
    }

    private inner class WordViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvEng: TextView = view.findViewById(R.id.tv_eng)
        val tvRus: TextView = view.findViewById(R.id.tv_rus)
    }
}